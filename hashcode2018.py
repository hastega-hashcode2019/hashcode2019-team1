import sys

rides = []
vehicles = []
rows = 0
columns = 0
vehicles_num = 0
rides_num = 0
bonus = 0
steps = 0


class Point:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __repr__(self):
        return str(self.x) + ","+str(self.y)

class Ride:
    def __init__(self, si, fi, es, lf, n):
        self.start_intersection = si
        self.finish_intersection = fi
        self.earliest_start = int(es)
        self.latest_finish = int(lf)
        self.distance = calc_distance(si, fi)
        self.number = n

class Vehicle:
    def __init__(self, n):
        self.actual_position = Point(0, 0)
        self.destination = None
        self.remain_steps = 0
        self.rides = []
        self.number = n

def calc_distance(start, end):
    return int(abs(start.x-end.x)+abs(start.y-end.y))

def init_vehicles():
    for i in range(vehicles_num):
        vehicles.append(Vehicle(i))

def decr_vehicles_step():
    for vehicle in vehicles:
        if vehicle.remain_steps > 0:
            vehicle.remain_steps -= 1

def generate_output(filename):
    outputfile = open(filename+".out", "w")
    for vehicle in vehicles:
        outputfile.write(str(len(vehicle.rides)))
        for rids in vehicle.rides:
            outputfile.write(" "+str(rids))
        outputfile.write("\n")
    outputfile.close()

def find_best(vehicle, step):
    max = 0.0
    best_ride = None
    total_cost = 0.0
    distance = 0
    if len(rides) == 0:
        return None, 0
    best_diff = sys.maxsize
    for ride in rides:
        diff = calc_distance(vehicle.actual_position, ride.start_intersection)
        if (step+diff+ride.distance)>ride.latest_finish or (step+diff+ride.distance)>=steps:
            continue
        if diff <= best_diff:
            best_diff = diff
            best_ride = ride
        # transfer_cost = calc_distance(vehicle.actual_position, ride.start_intersection)
        # distance = ride.distance
        # #if distance>1425:
        # #    continue
        # delay = ride.earliest_start - step
        # if delay >= 0:
        #     total_cost = transfer_cost + delay
        #     distance += bonus
        # else:
        #     total_cost = transfer_cost
        # #if (step+transfer_cost+ride.distance)>=ride.latest_finish:
        # #    continue
        # if total_cost == 0:
        #     return ride, (total_cost+ride.distance)
        # if(vehicle.number%4==0):
        #     score = 1.0/float(distance)
        # elif vehicle.number%4==1:
        #     score = float(distance)
        # elif vehicle.number%4==2:
        #     score = float(distance)/(float(total_cost)+0.1)
        # else:
        #     score = 1/(abs(1425-distance)+0.1)
        # if score > max:
        #     max = score
        #     best_ride = ride
        # #print max
    if best_ride==None:
        return None, 0
    return best_ride, (best_diff+best_ride.distance)




def assign_ride(vehicle, ride, r_steps):
    vehicle.rides.append(ride.number)
    rides.remove(ride)
    vehicle.remain_steps = r_steps
    vehicle.actual_position = ride.finish_intersection




filename = "input/e_high_bonus.in"
textfile = open(filename, "r").read()
textfile = textfile.split('\n')
firstRow = textfile[0].split(' ')
rows = int(firstRow[0])
columns = int(firstRow[1])
vehicles_num = int(firstRow[2])
rides_num = int(firstRow[3])
bonus = int(firstRow[4])
steps = int(firstRow[5])
for line in range(1, len(textfile)-1):
    cells = textfile[line].split(" ")
    rid = Ride(Point(cells[0],cells[1]), Point(cells[2],cells[3]),cells[4],cells[5], int(line-1))
    rides.append(rid)
init_vehicles()



#print str(rides_num) + " "+str(len(rides))
#test_input()
for step in range(steps):
    print step
    for vehicle in vehicles:
        if vehicle.remain_steps == 0:
            best_ride, remain_steps = find_best(vehicle, step)
            if best_ride == None:
                continue
            #print str(best_ride.distance)
            assign_ride(vehicle, best_ride, remain_steps)
    decr_vehicles_step()



generate_output(filename)

