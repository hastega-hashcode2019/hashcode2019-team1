nOfPhoto = 0
photos = []

class Tag:
    def __init__(self, tag):
        self.tag = tag

    def __eq__(self, other):
        return self.tag == other.tag

    def __repr__(self):
        return str(self.tag)

class Photo:
    def __init__(self, o, n):
        self.orientation = o
        self.nOfTags = n
        self.tags = []

    def __repr__(self):
        strTags = ""
        for tag in self.tags:
            strTags+=str(tag)+" "
        return str(self.orientation)+" "+str(self.nOfTags)+" "+strTags


filename = "input/b_lovely_landscapes.txt"
textfile = open(filename, "r").read()
textfile = textfile.split('\n')
firstRow = textfile[0]
nOfPhoto = int(firstRow)
for line in range(1, len(textfile)-1):
    cells = textfile[line].split(" ")
    orientation = str(cells[0])
    nOftags = str(cells[1])
    photo = Photo(orientation, nOftags)
    for cell in range(2, len(cells)):
        tag = Tag(str(cells[cell]))
        photo.tags.append(tag)
    photos.append(photo)

for photo in photos:
    print photo

